Feature: Manage bookings
  In order to [goal]
  [stakeholder]
  wants [behaviour]

  Scenario: Register new booking
    Given I am on the new booking page
    When I fill in "Name" with "name 1"
    And I choose "2019-11-03" in "Date" date select
    And I fill in "Days" with "1"
    And I select "Grand Garden Pavilion King Room" from "roomtype"
    And I select "1" from "booking_peoples"
    And I fill in "Phone" with "0868158118"
    And I press "Book"
    Then I should see "name 1"
    And I should see "2019-11-03"
    And I should see "1"
    And I should see "1"
    And I should see "Grand Garden Pavilion King Room"
    And I should see "0868158118"
    And I should see the Edit button.
    And I should see the Back button.
