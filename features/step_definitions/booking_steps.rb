Given /^the following bookings:$/ do |bookings|
  Booking.create!(bookings.hashes)
end

When /^I delete the (\d+)(?:st|nd|rd|th) booking$/ do |pos|
  visit bookings_path
  within("table tr:nth-child(#{pos.to_i})") do
    click_link "Destroy"
  end
end

Then /^I should see the following bookings:$/ do
end
