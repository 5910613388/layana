class Booking < ApplicationRecord
  def self.find_in_bkdb(search_terms)
    if search_terms
      bookings = Booking.find_by(name: search_terms)
      if bookings
        self.where(name: bookings.name)
      else
        return 7
      end
    else
      return nil
    end
  end
end