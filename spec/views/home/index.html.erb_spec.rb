require 'rails_helper'

RSpec.describe "home/index", type: :view do
  it "renders Title" do
    render
    expect(rendered).to match("Layana")
    expect(rendered).to have_css('body .layana')
  end

  it "renders content as table" do
    render
    expect(rendered).to have_table('table_content')
  end

  it 'renders top navigation bar and link to other page' do
    render
    have_link 'Home', href: "/"
    have_link 'Booking', href: "/bookings/index"
    have_link 'About', href: "/about/index"
  end

  it 'should have images as content' do
    render
    expect(rendered).to have_css("img[src*='p']")
  end
end
