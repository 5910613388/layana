require 'rails_helper'

RSpec.describe Booking, type: :model do
  describe 'searching bookingdb by keyword' do
    it 'calls bookingdb with name keywords' do
      expect(Booking).to receive(:find_by).with({:name=>"Tanapol Boonmak"})
      Booking.find_in_bkdb('Tanapol Boonmak')
    end
  end
end
