require 'rails_helper'
RSpec.describe BookingsController, type: :controller do

  let(:valid_attributes) {
    {name: 'Tanapol Boonmak', date: Date.new, days: 1, peoples: 1, room_type: '1', phone: '0868158118'}
  }

  let(:invalid_attributes) {
    {virus: 'mybnv'}
  }

  describe "GET #index" do
    before :each do
      @fake_result = [double('booking1'), double('booking2')]
    end
    it 'calls the model method that performs BKDb search' do
      expect(Booking).to receive(:find_in_bkdb).with('Tanapol Boonmak').and_return(@fake_result)
      get :index, params: {:search_terms => 'Tanapol Boonmak'}
    end
    describe 'after valid search' do
      before :each do
        allow(Booking).to receive(:find_in_bkdb).and_return(@fake_result)
        get :index, params: {:search_terms => 'Tanapol Boonmak'}
      end
      it 'select the Search Result template for rendering' do
        expect(response).to render_template('index')
      end
      it 'makes the BKDb search results available to that template' do
        expect(assigns(:bookings)).to eq(@fake_result)
      end
    end
  end

  describe "GET #show" do
    it "returns a success response" do
      booking = Booking.create! valid_attributes
      get :show, params: {id: booking.to_param}
      expect(response).to be_successful
    end
  end

  describe "GET #new" do
    it "returns a success response" do
      get :new, params: {}
      expect(response).to be_successful
    end
  end

  describe "GET #edit" do
    it "returns a success response" do
      booking = Booking.create! valid_attributes
      get :edit, params: {id: booking.to_param}
      expect(response).to be_successful
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Booking" do
        expect {
          post :create, params: {booking: valid_attributes}
        }.to change(Booking, :count).by(1)
      end

      it "redirects to the created booking" do
        post :create, params: {booking: valid_attributes}
        expect(response).to redirect_to(Booking.last)
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'new' template)" do
        post :create, params: {booking: invalid_attributes}
        expect(response).to have_http_status(302)
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        {name: 'Kosit Sukbanterng', date:Date.new, days: 2, peoples: 2, room_type: '2', phone: '0955175287'}
      }

      it "updates the requested booking" do
        booking = Booking.create! valid_attributes
        put :update, params: {id: booking.to_param, booking: new_attributes}
        booking.reload
        expect(assigns(:booking).attributes['name']).to match(new_attributes[:name])
        expect(assigns(:booking).attributes['date']).to match(new_attributes[:date])
        expect(assigns(:booking).attributes['days']).to match(new_attributes[:days])
        expect(assigns(:booking).attributes['peoples']).to match(new_attributes[:peoples])
        expect(assigns(:booking).attributes['room_type']).to match(new_attributes[:room_type])
        expect(assigns(:booking).attributes['phone']).to match(new_attributes[:phone])
      end

      it "redirects to the booking" do
        booking = Booking.create! valid_attributes
        put :update, params: {id: booking.to_param, booking: valid_attributes}
        expect(response).to redirect_to(booking)
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'edit' template)" do
        booking = Booking.create! valid_attributes
        put :update, params: {id: booking.to_param, booking: invalid_attributes}
        expect(response).to have_http_status(302)
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested booking" do
      booking = Booking.create! valid_attributes
      expect {
        delete :destroy, params: {id: booking.to_param}
      }.to change(Booking, :count).by(-1)
    end

    it "redirects to the bookings list" do
      booking = Booking.create! valid_attributes
      delete :destroy, params: {id: booking.to_param}
      expect(response).to redirect_to(bookings_url)
    end
  end
end
