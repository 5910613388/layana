class CreateBookings < ActiveRecord::Migration[6.0]
  def change
    create_table :bookings do |t|
      t.string :name
      t.date :date
      t.integer :days
      t.integer :peoples
      t.string :room_type
      t.string :phone

      t.timestamps
    end
  end
end
